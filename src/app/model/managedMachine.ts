import {AppUser} from './appUser';
export interface ManagedMachine {
  id: number;
  apiKey: string;
  jmxURL: string;
  name: string;
  user: AppUser;
  configuration: string;
  onDashboard: boolean;
}
