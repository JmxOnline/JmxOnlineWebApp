import {Role} from './role';

export class AppUser {
  idUser: number;
  username: string;
  password: string;
  firstName: string;
  lastName: string;
  enabled: number;
  email: string;
  roles: Role[];
}
