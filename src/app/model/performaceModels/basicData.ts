export interface BasicData {
  name: string;
  arch: string;
  committedVirtualMemorySize: number;
  freePhysicalMemorySize: number;
  freeSwapSpaceSize: number;
  processCpuLoad: number;
  processCpuTime: number;
  systemCpuLoad: number;
  totalPhysicalMemorySize: number;
  totalSwapSpaceSize: number;
  availableProcessors: number;
}
