export interface LockedStackFrame {
  methodName;
  fileName;
  lineNumber;
  className;
  nativeMethod;
}
