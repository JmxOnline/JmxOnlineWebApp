import {LockedStackFrame} from './lockedStackFrame';

export interface StackTraceLockMonitor {
  className;
  identityHashCode;
  lockedStackDepth;
  lockedStackFrame: LockedStackFrame;
}
