export enum PerformanceTypes {
   viewCPU, viewMemory, viewThreads, viewClassInfo
}
