export interface StackTraceElement {
  methodName;
  fileName;
  lineNumber;
  className;
  nativeMethod;
}
