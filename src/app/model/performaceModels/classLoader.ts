export  interface  ClassLoader {
  loadedClassCount;
  totalLoadedClassCount;
  unloadedClassCount;
}
