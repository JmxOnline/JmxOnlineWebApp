import {StackTraceElement} from './stackTraceElement';
import {StackTraceLockMonitor} from './stackTraceLockMonitor';

export interface Thread {
  threadState;
  threadName;
  threadID;
  blockedCount;
  stackTraceElements: StackTraceElement[];
  stackTraceLockMonitors: StackTraceLockMonitor[];
}
