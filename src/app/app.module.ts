import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

import { AppComponent } from './app.component';
import {MatButtonModule} from '@angular/material';
import {RoutingModule} from './shared/routing/routing.module';
import {ContainerModule} from './container/container.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import JmxService from './shared/service/jmxService';
import { InjectDirective } from './inject.directive';
import {InterceptService} from './shared/service/intercept.service';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';



@NgModule({
  declarations: [
    AppComponent,
    InjectDirective,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RoutingModule,
    ContainerModule,
    MatButtonModule,
    HttpClientModule
  ],
  providers: [ JmxService, InterceptService, {provide: HTTP_INTERCEPTORS, useClass: InterceptService, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
