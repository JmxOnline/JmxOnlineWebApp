import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMachineScreenComponent } from './add-machine-screen.component';

describe('AddMachineScreenComponent', () => {
  let component: AddMachineScreenComponent;
  let fixture: ComponentFixture<AddMachineScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddMachineScreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMachineScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
