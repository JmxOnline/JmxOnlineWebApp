import {Component, EventEmitter, Inject, Input, OnChanges, OnInit, Output, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ManagedMachine} from '../../model/managedMachine';


@Component({
  selector: 'app-add-machine-screen',
  templateUrl: './add-machine-screen.component.html',
  styleUrls: ['./add-machine-screen.component.css']
})
export class AddMachineScreenComponent implements OnInit {

  constructor(private _fb: FormBuilder,
  public dialogRef: MatDialogRef<AddMachineScreenComponent>,
@Inject(MAT_DIALOG_DATA) public data: any) { }

  @Output()
  createMachine: EventEmitter<ManagedMachine> = new EventEmitter<ManagedMachine>();
  @Input()
  jmxKey: string;

  isLeftVisible = true;
  machineFormGroup: FormGroup;
  @ViewChild('continueSubmit') continueSubmit: HTMLElement;
  ngOnInit() {

    this.machineFormGroup = this._fb.group({
      machineName: ['', Validators.required],
    });

    if (this.jmxKey !== '') {
      this.isLeftVisible = false;
    }
  }

  dialogClose() {
    this.data.jmxKey = this.jmxKey;
    this.dialogRef.close(this.jmxKey);
  }
  submitForm() {

    const m: ManagedMachine = {
      apiKey: '',
      name: this.machineFormGroup.get('machineName').value,
      id: 0,
      jmxURL: '',
      user: null,
      onDashboard: true,
      configuration: '',
    };
    this.createMachine.emit(m);

    
    this.isLeftVisible = false;
  }
}
