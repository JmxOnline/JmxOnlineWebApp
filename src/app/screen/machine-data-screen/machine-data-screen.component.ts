import {

  Component,
  ComponentFactoryResolver, ComponentRef, EventEmitter, Input, OnDestroy,
  OnInit, Output,
  ViewChild, ViewContainerRef
} from '@angular/core';
import MachineSocketService from '../../shared/service/machineSocketService';

import JmxService from '../../shared/service/jmxService';
import {OperatingSystemBean} from '../../model/performaceModels/operatingSystemBean';
import {Message} from '@stomp/stompjs';
import {CPUComponent} from '../custom-components/cpu/cpu.component';
import {RAMComponent} from '../custom-components/ram/ram.component';
import {ActivatedRoute} from '@angular/router';
import {ManagedMachine} from '../../model/managedMachine';
import ConfigurationService from '../../shared/service/configurationService';
import {PerformanceTypes} from '../../model/performaceModels/performanceTypes';
import {MatFormFieldAppearance, MatOptionSelectionChange} from '@angular/material';

@Component({
  selector: 'app-machine-data-screen',
  templateUrl: './machine-data-screen.component.html',
  styleUrls: ['./machine-data-screen.component.css']
})
export class MachineDataScreenComponent implements OnInit, OnDestroy {
  systemData: OperatingSystemBean;
  JSON: any;
  os = '';
  arch = '';

  dataAvailableMachine = false;
  connectionStatusMachine = 'CLOSED';
  statusColorMachine = '#ef5350';
  connectionStatus = 'CLOSED';
  connectionStatusColor = '#ef5350';
  children: ComponentRef<any>[] = new Array();
  performanceTypes  = PerformanceTypes;
  subscription;
  stopSubscription;
  ping;
  @Output()
  saveConfiguration: EventEmitter<ManagedMachine> = new EventEmitter<ManagedMachine>();

  @Input()
  machine: ManagedMachine;
  @Input()
  showSave = false;
  isError = false;
  error = null;

  performanceKeys(): Array<string> {
    const keys = Object.keys(this.performanceTypes);
    return keys.slice(keys.length / 2);
  }

  @ViewChild('rowWrap', { read: ViewContainerRef }) myRef;
  @ViewChild('fullScreen', { read: ViewContainerRef }) myRefFullScreen;
  constructor(private machineDataService: MachineSocketService,
              private jmxService: JmxService,
              private configurationService: ConfigurationService,
              private machineSocketService: MachineSocketService,
              private _componentFactoryResolver: ComponentFactoryResolver) {
    this.JSON = JSON;
  }
  addPerfItem(data: MatOptionSelectionChange) {

    if (data.isUserInput) {
      this.addComponent(data.source.getLabel());
      data.source.deselect();
    }
  }


  ngOnInit() {
    this.machineSocketService.getSocketState().subscribe(data => {
      this.connectionStatus = data;

      data === 'CONNECTED' ? this.connectionStatusColor = '#FCAB1E' : this.connectionStatusColor = '#ef5350';
    });
    if (this.machine.configuration !== '') {
      this.children = this.configurationService.assembleConfiguration(this.machine, this.myRef, this.myRefFullScreen);
    }
    this.jmxService.startConnection('BASIC_PERFORMANCE', this.machine.apiKey)
      .subscribe((data) => {
        this.machineDataService.connectOperatingSystem(this.machine.apiKey);
        this.machineSocketService.connectStop(this.machine.apiKey);
        this.subscription = this.machineDataService.getOperatingSystemData().subscribe((dataMes) => {
          this.onData(dataMes);
          this.startPing();
        });

        this.stopSubscription = this.machineDataService.getStop().subscribe((dataMes) => {
          this.onStop(dataMes);
          this.stopPing();
        });


      }, (error) => {
          this.error = error;
          this.isError = true;
      });
  }

  onStop(dataMsg) {

    if (dataMsg) {
      this.machineSocketService.disconnect();
      this.isError = true;
      this.error = {error: {message: 'Lost connection to machine,please check if the app is running'}};
    }
  }

  startPing() {
   this.ping = setInterval(() => {this.jmxService.pingConnection(this.machine.apiKey); }, 1620000);
  }

  stopPing() {
    if (this.ping) {
      clearInterval(this.ping);
    }
  }
  onData(dataMsg) {

    const obj = JSON.parse(dataMsg.body);

    this.os = obj.name;
    this.arch = obj.arch.includes(64) ? this.arch = '64bit' : '32bit';

    this.dataAvailableMachine = true;
    this.connectionStatusMachine = 'CONNECTED';
    this.statusColorMachine = '#FCAB1E';
    this.subscription.unsubscribe();
  }
  addComponent(event) {
   const child = this.configurationService.addNewItem(event, this.myRef, this.myRefFullScreen, this.machine.apiKey);
   if (child) {
     this.children.push(child);
   }
  }

  private onStateChange(state: string) {
  }

  saveConfig() {

    let configuration = '';
    this.children.forEach((child, index) => {
      configuration += child.instance.name;
      configuration += ';';
});
    this.machine.configuration = configuration;
    console.log(configuration);
    this.saveConfiguration.emit(this.machine);
  }

  ngOnDestroy(): void {
    this.children.forEach(child => {
      child.destroy();
    });
    this.stopPing();
    this.jmxService.closeConnection('ALL', this.machine);
  }
}
