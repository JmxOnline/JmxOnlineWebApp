import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MachineDataScreenComponent } from './machine-data-screen.component';

describe('MachineDataScreenComponent', () => {
  let component: MachineDataScreenComponent;
  let fixture: ComponentFixture<MachineDataScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MachineDataScreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachineDataScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
