import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MachineListScreenComponent } from './machine-list-screen.component';

describe('MachineListScreenComponent', () => {
  let component: MachineListScreenComponent;
  let fixture: ComponentFixture<MachineListScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MachineListScreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachineListScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
