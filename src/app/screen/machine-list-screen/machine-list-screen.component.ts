import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ManagedMachine} from '../../model/managedMachine';

@Component({
  selector: 'app-machine-list-screen',
  templateUrl: './machine-list-screen.component.html',
  styleUrls: ['./machine-list-screen.component.css']
})
export class MachineListScreenComponent implements OnInit {


  @Input()
  machineList: ManagedMachine[];

  @Output()
  onDashboardChanged = new EventEmitter<ManagedMachine>();
  constructor() { }

  ngOnInit() {
  }


  onDashboardChange(machine: ManagedMachine){
    this.onDashboardChanged.emit(machine);

  }
}
