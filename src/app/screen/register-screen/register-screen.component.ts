import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {AppUser} from '../../model/appUser';
import {ErrorStateMatcher} from '@angular/material/core';

export class PasswordStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-register-screen',
  templateUrl: './register-screen.component.html',
  styleUrls: ['./register-screen.component.css']
})
export class RegisterScreenComponent implements OnInit {


  @Output()
  register: EventEmitter<AppUser> = new EventEmitter<AppUser>();
  minlength = 5;

  constructor(private _fb: FormBuilder) {
  }

  registerFormGroup: FormGroup;

  email;
  username;
  password;
  matcher = new PasswordStateMatcher();

  ngOnInit() {
    this.registerFormGroup = this._fb.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      username: ['', Validators.required],
      password: ['', Validators.compose([Validators.required, Validators.minLength(this.minlength)])],
    });

    this.email = this.registerFormGroup.get('email');
    this.username = this.registerFormGroup.get('username');
    this.password = this.registerFormGroup.get('password');
  }


  submitForm(e) {
    e.preventDefault();
    const email = this.registerFormGroup.controls.username.value;
    const username = this.registerFormGroup.controls.username.value;
    const password = this.registerFormGroup.controls.password.value;
    if (this.registerFormGroup.valid) {
      const user: AppUser = new AppUser();
      user.email = email;
      user.username = username;
      user.password = password;


      this.register.emit(user);
    }

  }
}
