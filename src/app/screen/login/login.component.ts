import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AppUser} from '../../model/appUser';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  @Output()
  login: EventEmitter<AppUser> = new EventEmitter<AppUser>();
  @Input()
  errorData: any;
  JSON: any;

  constructor(private _fb: FormBuilder) {this.JSON = JSON; }

  loginFormGroup: FormGroup;
  ngOnInit() {
    this.loginFormGroup = this._fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }


  submitForm(e) {

    const username = this.loginFormGroup.controls.username.value;
    const password = this.loginFormGroup.controls.password.value;
    if (this.loginFormGroup.valid) {
      const user: AppUser = new AppUser();
      user.username = username;
      user.password = password;


      this.login.emit(user);
    }

  }
}
