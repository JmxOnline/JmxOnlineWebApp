import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetStartedScreenComponent } from './get-started-screen.component';

describe('GetStartedScreenComponent', () => {
  let component: GetStartedScreenComponent;
  let fixture: ComponentFixture<GetStartedScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetStartedScreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetStartedScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
