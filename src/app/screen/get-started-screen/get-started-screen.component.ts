import { Component, OnInit } from '@angular/core';
import {AddMachineContainerComponent} from '../../container/add-machine-container/add-machine-container.component';
import {MatDialog} from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-get-started-screen',
  templateUrl: './get-started-screen.component.html',
  styleUrls: ['./get-started-screen.component.css']
})
export class GetStartedScreenComponent implements OnInit {

  jmxKey = '';
  constructor(public dialog: MatDialog,
              private active: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
  }
  addNewMachine() {
    const dialogRef = this.dialog.open(AddMachineContainerComponent, {
      width: '600px',
      data: {jmxKey: ''}
    });
    dialogRef.afterClosed().subscribe(result => {

      this.jmxKey = result;
    });
  }

  openDownload(){
    window.open('http://localhost:8080/jmxOnlineApi/jmx/jmxSDK.jar');
  }
}
