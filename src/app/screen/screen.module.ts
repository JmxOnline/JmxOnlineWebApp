import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import {MaterialHandlerModule} from '../shared/material-handler/material-handler.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import {ReactiveFormsModule} from '@angular/forms';
import { HeaderScreenComponent } from './header-screen/header-screen.component';
import { MachineListScreenComponent } from './machine-list-screen/machine-list-screen.component';
import { MachineListItemComponent } from './custom-components/machine-list-item/machine-list-item.component';
import {RouterModule} from '@angular/router';
import { MachineDataScreenComponent } from './machine-data-screen/machine-data-screen.component';
import { CPUComponent } from './custom-components/cpu/cpu.component';
import {Ng2GoogleChartsModule} from 'ng2-google-charts';
import { RAMComponent } from './custom-components/ram/ram.component';
import { AddMachineScreenComponent } from './add-machine-screen/add-machine-screen.component';

import { SlidePanelComponent } from './custom-components/slide-panel/slide-panel.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AddMachineContainerComponent} from '../container/add-machine-container/add-machine-container.component';
import {DashboardScreenComponent} from './dashboard-screen/dashboard-screen.component';
import {MaterialCardComponent} from './custom-components/material-card/material-card.component';
import {ChartsModule, MDBBootstrapModule} from 'angular-bootstrap-md';
import ConfigurationService from '../shared/service/configurationService';
import { GetStartedScreenComponent } from './get-started-screen/get-started-screen.component';
import {ContainerModule} from '../container/container.module';
import {ThreadsComponent} from './custom-components/threads/threads.component';
import { RegisterScreenComponent } from './register-screen/register-screen.component';
import { ClassLoaderComponent } from './custom-components/class-loader/class-loader.component';
import { UniversalDialogComponent } from './custom-components/universal-dialog/universal-dialog.component';
@NgModule({
  imports: [
    CommonModule,
    MaterialHandlerModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    RouterModule,
    Ng2GoogleChartsModule,
    BrowserAnimationsModule,
    MDBBootstrapModule.forRoot()
  ],
  declarations: [LoginComponent,
    HeaderScreenComponent,
    MachineListScreenComponent,
    MachineListItemComponent,
    MachineDataScreenComponent,
    CPUComponent,
    RAMComponent,
    AddMachineScreenComponent,
    SlidePanelComponent,
    DashboardScreenComponent,
    MaterialCardComponent,
    GetStartedScreenComponent,
    ThreadsComponent,
    RegisterScreenComponent,
    ClassLoaderComponent,
    UniversalDialogComponent
  ],
  providers: [ConfigurationService],
  entryComponents:[CPUComponent, RAMComponent, ClassLoaderComponent, ThreadsComponent, AddMachineContainerComponent],
  exports: [
    LoginComponent,
    HeaderScreenComponent,
    MachineListScreenComponent,
    MachineDataScreenComponent,
    AddMachineScreenComponent,
    SlidePanelComponent,
    DashboardScreenComponent,
    GetStartedScreenComponent,
    RegisterScreenComponent
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
})
export class ScreenModule { }
