import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ManagedMachine} from '../../model/managedMachine';
import {AddMachineContainerComponent} from '../../container/add-machine-container/add-machine-container.component';
import {MatDialog} from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-dashboard-screen',
  templateUrl: './dashboard-screen.component.html',
  styleUrls: ['./dashboard-screen.component.scss']
})
export class DashboardScreenComponent implements OnInit {

  @Input()
  dashboardMachines: ManagedMachine[];

  constructor(public dialog: MatDialog,
              private active: ActivatedRoute,
              private router: Router) { }
  ngOnInit() {
  }


  onGetStarted() {

  }


}
