import { Component, OnInit } from '@angular/core';
import {AddMachineScreenComponent} from '../add-machine-screen/add-machine-screen.component';
import {MatDialog} from '@angular/material';
import {AddMachineContainerComponent} from '../../container/add-machine-container/add-machine-container.component';
import {ActivatedRoute, ActivatedRouteSnapshot, NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-header-screen',
  templateUrl: './header-screen.component.html',
  styleUrls: ['./header-screen.component.css']
})
export class HeaderScreenComponent implements OnInit {
  opened = false;
  topBarHidden = false;
  constructor(public dialog: MatDialog,
              private active: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.router.events.subscribe((value => {

      if (value instanceof NavigationEnd) {
        if (value.url === '/login' || value.url === '/register') {
          this.topBarHidden = true;
        } else {
          this.topBarHidden = false;
        }
      }
    }));
  }

  onAddMachineClick() {

      const dialogRef = this.dialog.open(AddMachineContainerComponent, {
        width: '600px',
        data: {}
      });

    dialogRef.afterClosed().subscribe(result => {

      if (this.router.url === '/machineList') {
        window.location.reload();
      } else {
        this.router.navigateByUrl('/machineList');
      }


    });
  }

}
