///<reference path="../../../../../node_modules/@angular/core/src/metadata/directives.d.ts"/>
import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import JmxService from '../../../shared/service/jmxService';
import MachineSocketService from '../../../shared/service/machineSocketService';
import {Thread} from '../../../model/performaceModels/thread';

@Component({
  selector: 'app-threads',
  templateUrl: './threads.component.html',
  styleUrls: ['./threads.component.css']
})
export class ThreadsComponent implements OnInit, OnDestroy {
  isCollapsed = true;
  dataAvailable = false
  theadData: Thread[] = new Array();
  privateSub = null;
  curentClass = 'notBlocked';
  trackByIndex = index => index;
  @Input()
  apiKey: string;
  @Input()
  selfRef: any;
  @Input()
  name: string;
  constructor(private jmxService: JmxService,
              private machineData: MachineSocketService) { }
  ngOnInit() {

    this.jmxService.startConnection('THREAD_DATA', this.apiKey)
      .subscribe((data => {
       this.machineData.connectThreads(this.apiKey);
        this.privateSub = this.machineData.getThreadsData().subscribe((datames) => {
          this.onData(datames);
        });
      }));

  }
  onData(dataMess) {
    const obj: Thread[] = JSON.parse(dataMess.body);
    this.theadData = obj;
    if (!this.dataAvailable) {this.dataAvailable = true; }


  }

  destroy(){
    this.selfRef.destroy();
  }

  ngOnDestroy(): void {
    this.privateSub.unsubscribe();
  }


}
