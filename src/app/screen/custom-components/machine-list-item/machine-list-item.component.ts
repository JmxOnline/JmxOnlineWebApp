import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';
import {ManagedMachine} from '../../../model/managedMachine';

@Component({
  selector: 'app-machine-list-item',
  templateUrl: './machine-list-item.component.html',
  styleUrls: ['./machine-list-item.component.css']
})
export class MachineListItemComponent implements OnInit {

  constructor(private _router: Router) { }

  @Input()
  managedMachine: ManagedMachine;

  @Output()
  onDashBoardChange = new EventEmitter<ManagedMachine>();
  ngOnInit() {
  }



  switchOnDashboard(){

    this.managedMachine.onDashboard = !this.managedMachine.onDashboard;

    this.onDashBoardChange.emit(this.managedMachine);
  }

}
