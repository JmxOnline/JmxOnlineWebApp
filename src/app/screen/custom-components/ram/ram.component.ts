import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {OperatingSystemBean} from '../../../model/performaceModels/operatingSystemBean';
import MachineSocketService from '../../../shared/service/machineSocketService';
import {Message} from '@stomp/stompjs';
import JmxService from '../../../shared/service/jmxService';
import {BasicData} from '../../../model/performaceModels/basicData';

@Component({
  selector: 'app-ram',
  templateUrl: './ram.component.html',
  styleUrls: ['./ram.component.css']
})
export class RAMComponent implements OnInit,OnDestroy {

  constructor(private machineDataService: MachineSocketService,
              private jmxService: JmxService) {
  }
  privateSub = null;
  @ViewChild('cchart') cchart;
  systemData: BasicData;
  ramData: any;
  @Input()
  apiKey: string;
  @Input()
  selfRef: any;
  @Input()
  name: string;

  dataAvailable = false;


  destroy() {
    this.selfRef.destroy();
  }
  ngOnInit() {

    this.ramData = {
      chartType: 'PieChart',
      dataTable: [
        ['Label', 'Value'],
        ['Free', 50],
        ['Used', 50],
      ],
      options: {
        width: 400, height: 200,
        pieHole: 0.4
      }
    };
    this.jmxService.startConnection('BASIC_PERFORMANCE', this.apiKey)
     .subscribe((data) => {
        this.machineDataService.connectOperatingSystem(this.apiKey);
       this.privateSub = this.machineDataService.getOperatingSystemData().subscribe((dataMes) => {
          this.onData(dataMes);
        });
     });
  }

// todo remove this once api is fully realised
  private onData(message: Message) {

    const obj = JSON.parse(message.body);
    this.systemData = obj;
    const memdif = (this.systemData.freePhysicalMemorySize / this.systemData.totalPhysicalMemorySize);
    const percent = (Math.round(memdif * 100) / 100) * 100;
    this.ramData.dataTable[1][1] = percent;
    this.ramData.dataTable[2][1] = 100 - percent;
    this.cchart.wrapper.setDataTable(this.ramData.dataTable);
    this.cchart.redraw();
    if (!this.dataAvailable) {this.dataAvailable = true; }
  }


  private onStateChange(state: string) {
    console.log(JSON.stringify(state));
  }

  ngOnDestroy(): void {
    this.privateSub.unsubscribe();
  }
}
