import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

export interface DialogData {
  text: string;
  title: string;
  isActionOK: boolean;
  isActionCancel: boolean;
  showCancel: boolean;
}

@Component({
  selector: 'app-universal-dialog',
  templateUrl: './universal-dialog.component.html',
  styleUrls: ['./universal-dialog.component.css']
})
export class UniversalDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<UniversalDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  ngOnInit() {
  }
  clickOK() {
    this.data.isActionOK = true;
    this.data.isActionCancel = false;
    this.dialogRef.close(this.data);
  }
  clickCancel() {
    this.data.isActionOK = false;
    this.data.isActionCancel = true;
    this.dialogRef.close(this.data);
  }

}
