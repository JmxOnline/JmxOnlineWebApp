import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import JmxService from '../../../shared/service/jmxService';
import {BasicData} from '../../../model/performaceModels/basicData';
import MachineSocketService from '../../../shared/service/machineSocketService';
import {Message} from '@stomp/stompjs';
import {ClassLoader} from '../../../model/performaceModels/classLoader';

@Component({
  selector: 'app-class-loader',
  templateUrl: './class-loader.component.html',
  styleUrls: ['./class-loader.component.css']
})
export class ClassLoaderComponent implements OnInit, OnDestroy {

  constructor(private machineDataService: MachineSocketService,
              private jmxService: JmxService) { }
  dataAvailable = false;
  percentCpu = 0;
  privateSub = null;
  @ViewChild('cchart') cchart;
  classLoaderData: ClassLoader;
  @Input()
  apiKey: string;
  @Input()
  selfRef: any;
  @Input()
  name: string;

  classData: any;
  ngOnInit() {
    this.classData = {
      chartType: 'PieChart',
      dataTable: [
        ['Label', 'Value'],
        ['Loaded', 50],
        ['Unloaded', 50],
      ],
      options: {
        width: 400, height: 200,
        pieHole: 0.4
      }
    };

    this.jmxService.startConnection('CLASS_LOADER_INFO', this.apiKey)
      .subscribe((data) => {
        this.machineDataService.connectClassLoader(this.apiKey);
        this.privateSub = this.machineDataService.getClassData().subscribe((dataMes) => {
          this.onData(dataMes);
        });
      });
  }

  private onData(message: Message) {
    const obj = JSON.parse(message.body);

  this.classLoaderData = obj;
  const percentageLoaded =
    (this.classLoaderData.loadedClassCount / this.classLoaderData.totalLoadedClassCount) * 100;
    const percentageUnLoaded =
      (this.classLoaderData.unloadedClassCount / this.classLoaderData.totalLoadedClassCount) * 100;
  this.classData.dataTable[1][1] = percentageLoaded;
  this.classData.dataTable[2][1] = percentageUnLoaded;


    this.cchart.wrapper.setDataTable(this.classData.dataTable);
    this.cchart.redraw();
    this.dataAvailable = true;
  }


  private onStateChange(state: string) {
    console.log(JSON.stringify(state));
  }

  destroy() {
    this.selfRef.destroy();
  }

  ngOnDestroy(): void {
    this.privateSub.unsubscribe();
  }
}
