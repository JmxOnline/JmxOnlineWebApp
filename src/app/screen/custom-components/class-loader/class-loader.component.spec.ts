import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassLoaderComponent } from './class-loader.component';

describe('ClassLoaderComponent', () => {
  let component: ClassLoaderComponent;
  let fixture: ComponentFixture<ClassLoaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassLoaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassLoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
