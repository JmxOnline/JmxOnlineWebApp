///<reference path="../../../../../node_modules/@angular/core/src/metadata/directives.d.ts"/>
import {Component, ComponentRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import MachineSocketService from '../../../shared/service/machineSocketService';
import {OperatingSystemBean} from '../../../model/performaceModels/operatingSystemBean';
import {Message} from '@stomp/stompjs';
import JmxService from '../../../shared/service/jmxService';
import {BasicData} from '../../../model/performaceModels/basicData';

@Component({
  selector: 'app-cpu',
  templateUrl: 'cpu.component.html',
  styleUrls: ['cpu.component.css']
})
export class CPUComponent implements OnInit, OnDestroy {

  constructor(private machineDataService: MachineSocketService,
              private jmxService: JmxService) { }
  dataAvailable = false;
  percentCpu = 0;
  privateSub = null;
  @ViewChild('cchart') cchart;
  systemData: BasicData;
  @Input()
  apiKey: string;
  @Input()
  selfRef: any;
  @Input()
  name: string;

  cpuData: any;
  ngOnInit() {
    this.cpuData = {
      chartType: 'Gauge',
      dataTable: [
        ['Label', 'Value'],
        ['CPU', 0],
      ],
      options: {
        redFrom: 90, redTo: 100,
        yellowFrom: 60, yellowTo: 90,
        minorTicks: 5
      }
    };

    this.jmxService.startConnection('BASIC_PERFORMANCE', this.apiKey)
      .subscribe((data) => {
        this.machineDataService.connectOperatingSystem(this.apiKey);
       this.privateSub = this.machineDataService.getOperatingSystemData().subscribe((dataMes) => {
          this.onData(dataMes);
        });
      });
  }

  private onData(message: Message) {
    const obj = JSON.parse(message.body);
    console.log(obj);
    this.systemData = obj;

    const percent = Math.round(this.systemData.systemCpuLoad * 100) / 100;
    this.cpuData.dataTable[1][1] = percent * 100;
    this.percentCpu = percent * 100;
    this.cchart.wrapper.setDataTable(this.cpuData.dataTable);
    this.cchart.redraw();
    this.dataAvailable = true;
  }


  private onStateChange(state: string) {
    console.log(JSON.stringify(state));
  }

  destroy() {
    this.selfRef.destroy();
  }

  ngOnDestroy(): void {
    this.privateSub.unsubscribe();
  }


}
