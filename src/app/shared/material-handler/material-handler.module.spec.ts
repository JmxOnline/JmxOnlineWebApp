import { MaterialHandlerModule } from './material-handler.module';

describe('MaterialHandlerModule', () => {
  let materialHandlerModule: MaterialHandlerModule;

  beforeEach(() => {
    materialHandlerModule = new MaterialHandlerModule();
  });

  it('should create an instance', () => {
    expect(materialHandlerModule).toBeTruthy();
  });
});
