import {Injectable} from '@angular/core';
import {AppUser} from '../../model/appUser';

import {HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {StompConfig} from '@stomp/ng2-stompjs';
@Injectable()
export class CurrentUserService {
  private apiKey: string;
  private  registeredUser: AppUser;

  constructor(private router: Router) {
  const api = sessionStorage.getItem('apiKey');
  const user = JSON.parse(sessionStorage.getItem('currentUser'));
    this.apiKey = '';
    this.registeredUser = new AppUser();
    if (api != null) {
      this.apiKey = api;
    }
    if (user != null) {
      this.registeredUser = user;
    }
  }
  getAuthHeader() {
    const headers = new HttpHeaders({'authorization': this.getUserKey(),
      'content-type': 'application/json'});

    return headers;
  }
  setUserKey(apikey: string) {
    this.apiKey = apikey;
    sessionStorage.setItem('apiKey', this.apiKey);
  }

  getUserKey() {

    return this.apiKey;
  }

  setUser(appUser: AppUser) {

    this.registeredUser = appUser;
    sessionStorage.setItem('currentUser', JSON.stringify(this.registeredUser));
  }

  getUserRoles() {
    return this.registeredUser.roles;
  }

  getRegisteredUser() {
    return this.registeredUser;
  }

  getUsername() {
    return this.registeredUser.username;
  }

  isUserLoggedIn() {
    const api = sessionStorage.getItem('apiKey');
    if (api != null && api !== '') {
      return true;
    }
    return false;
  }

  isUserAdmin() {

    if (this.registeredUser.roles[0].roleName === 'ROLE_ADMIN') {
      return true;
    } else {
      return false;
    }
  }

  logout() {
    sessionStorage.setItem('apiKey', '');
   sessionStorage.setItem('currentUser', '');
    this.setUser(null);
    this.apiKey = null;
    location.reload();
    this.router.navigate(['/login']);
  }





}
