import { Injectable } from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CurrentUserService} from './currentUserService';
import {tap} from 'rxjs/operators';
import {ModalService} from './ModalService';

@Injectable({
  providedIn: 'root'
})
export class InterceptService implements HttpInterceptor {

  constructor(private currentUserService: CurrentUserService,
              private modalService: ModalService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {


    req = req.clone({
      setHeaders: {
        Authorization: this.currentUserService.getUserKey(),
      }
    });

    return next.handle(req)
      .pipe(tap(event => {
      }, e => {

        console.log(JSON.stringify(e));
        if (e.status === 401) {
          this.modalService.openDialog('Your login session expired', 'ERROR 😢').afterClosed().subscribe((data) => {
            this.currentUserService.logout();
          });
        } else if (e.status === 0) {
          this.modalService.openDialog('Service appears to be down,try again later', 'ERROR 😢').afterClosed().subscribe((data) => {
            this.currentUserService.logout();
          });
        } else if (e.status === 500) {
          this.modalService.openDialog('Internal error occurred', 'ERROR 😢');
          this.currentUserService.logout();
        } else if (e.status === 400) {
          this.modalService.openDialog(e.error.message, 'ERROR 😢');
        }

      }));

  }
}
