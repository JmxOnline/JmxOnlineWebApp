import {Observable, of} from 'rxjs';
import {AppUser} from '../../model/appUser';
import {catchError} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {ApiUrlService} from './apiurl.service';
import {ManagedMachine} from '../../model/managedMachine';
import {CurrentUserService} from './currentUserService';
import {Injectable} from '@angular/core';
import {ResponseString} from '../../model/responseString';

@Injectable()
export class ManagedMachineService {

  constructor(private _http: HttpClient,
              private  apiUrlService: ApiUrlService) {}


  getMachinesForUser(): Observable<ManagedMachine[]> {
    return this._http.get<ManagedMachine[]>(this.apiUrlService.managedMachines, );

  }

  getMachineById(id: number): Observable<ManagedMachine> {

    return this._http.get<ManagedMachine>(this.apiUrlService.managedMachines + '/' + id, );
  }


  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console instead
      return of(result as T);
    };
  }
  createNewMachine(machine: ManagedMachine): Observable<ResponseString> {

    return this._http.post<ResponseString>(this.apiUrlService.managedMachines, machine, );

  }

  getDashboardMachines(): Observable<ManagedMachine[]> {

    return this._http.get<ManagedMachine[]>(this.apiUrlService.managedMachines + '/dashboard' );

  }

  saveConfiguration(machine: ManagedMachine): Observable<ResponseString> {

    return this._http.post<ResponseString>(this.apiUrlService.managedMachines, machine );

  }

  saveOnDashboard(machine: ManagedMachine): Observable<ResponseString> {

    return this._http.post<ResponseString>(this.apiUrlService.managedMachines, machine );

  }


}
