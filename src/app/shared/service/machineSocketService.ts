import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import { Message } from '@stomp/stompjs';
import {StompConfig, StompRService, StompService, StompState} from '@stomp/ng2-stompjs';
import {map} from 'rxjs/internal/operators';
import {CurrentUserService} from './currentUserService';
import {StompHeaders} from '@stomp/ng2-stompjs/src/stomp-headers';

@Injectable({
  providedIn: 'root'
})

export default  class MachineSocketService {

  public operatingSystemInfo: Observable<Message>;
  public threads: Observable<Message>;
  public classLoaderInfo: Observable<Message>;
  public stop: Observable<Message>;
  public wsstate: Observable<string>;
  url = 'ws://localhost:8080/jmxOnlineApi/performance-MM/websocket';
  stompConfig: StompConfig = {
    url: this.url,


    headers: {
    },


    heartbeat_in: 0,


    heartbeat_out: 20000,


    reconnect_delay: 0,


    debug: false
  };

  constructor(private socketRService: StompRService,
              private currentUserService: CurrentUserService) {}


  private establisConnection() {
    this.stompConfig.url = this.url;
    this.stompConfig.url += '?key=' + this.currentUserService.getUserKey();

    this.socketRService.config = this.stompConfig;
    this.socketRService.initAndConnect();
  }
  public connectToSocket() {
    if (this.wsstate) {
     this.wsstate.subscribe((state) => {
       if (state === 'CLOSED') {
         this.establisConnection();
       }
     });
    } else {
     this.establisConnection();
     this.connectState();
    }
  }

  public disconnect(){
    this.socketRService.disconnect();
  }
  public connectOperatingSystem(apiKey: string) {
    this.operatingSystemInfo = this.socketRService.subscribe('/performance/simplePerformanceData/' + apiKey);
  }

  public connectClassLoader(apiKey: string) {
    this.classLoaderInfo = this.socketRService.subscribe('/performance/classLoaderData/' + apiKey);
  }
  public connectThreads(apiKey: string) {
    this.threads = this.socketRService.subscribe('/performance/threads/' + apiKey);
  }
  public connectState() {
    this.wsstate = this.socketRService.state.pipe(map((state: number) => StompState[state]));
  }
  public connectStop(apiKey: string) {
    this.stop = this.socketRService.subscribe('/performance/stop/' + apiKey);
  }
  public getOperatingSystemData() {
    return this.operatingSystemInfo;
  }
  public getThreadsData() {
    return this.threads;
  }
  public getClassData() {
    return this.classLoaderInfo;
  }
  public getSocketState() {
    return this.wsstate;
  }
  public getStop(){
    return this.stop;
  }

}
