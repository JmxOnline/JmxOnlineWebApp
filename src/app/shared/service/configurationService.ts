import {ComponentFactoryResolver, ComponentRef, Injectable, ViewContainerRef} from '@angular/core';
import {ManagedMachine} from '../../model/managedMachine';
import {ThreadsComponent} from '../../screen/custom-components/threads/threads.component';
import {RAMComponent} from '../../screen/custom-components/ram/ram.component';
import {CPUComponent} from '../../screen/custom-components/cpu/cpu.component';
import {ClassLoaderComponent} from '../../screen/custom-components/class-loader/class-loader.component';

@Injectable()
export default class ConfigurationService {

  constructor( private _componentFactoryResolver: ComponentFactoryResolver) {}

  assembleConfiguration(machine: ManagedMachine, viewContainerRow: ViewContainerRef, viewContainerFullScreen: ViewContainerRef) {
    const config = machine.configuration.split(';');
   const children: ComponentRef<any>[] = new Array();
    config.forEach((element, index) => {

      const child = this.addNewItem(element, viewContainerRow, viewContainerFullScreen, machine.apiKey);
      if (child) {
        children.push(child);
      }
    });
    return children;
  }


  addNewItem(itemType: string, viewContainer: ViewContainerRef, viewContainerFullscreen: ViewContainerRef, apiKey: string) {
    if (itemType === 'viewCPU') {
      const componentFactory = this._componentFactoryResolver.resolveComponentFactory(CPUComponent);
      const cpuChild = viewContainer.createComponent(componentFactory);
      cpuChild.instance.apiKey = apiKey;
      cpuChild.instance.selfRef = cpuChild;
      cpuChild.instance.name = 'viewCPU';
      return cpuChild;

    } else if (itemType === 'viewMemory') {
      const componentFactory = this._componentFactoryResolver.resolveComponentFactory(RAMComponent);
      const ramChild = viewContainer.createComponent(componentFactory);
      ramChild.instance.apiKey = apiKey;
      ramChild.instance.selfRef = ramChild;
      ramChild.instance.name = 'viewMemory';
      return ramChild;
    } else if (itemType === 'viewThreads') {
      const componentFactory = this._componentFactoryResolver.resolveComponentFactory(ThreadsComponent);
      const threadChild = viewContainerFullscreen.createComponent(componentFactory);
      threadChild.instance.apiKey = apiKey;
      threadChild.instance.selfRef = threadChild;
      threadChild.instance.name = 'viewThreads';
      return threadChild;
    } else if (itemType === 'viewClassInfo') {
      const componentFactory = this._componentFactoryResolver.resolveComponentFactory(ClassLoaderComponent);
      const classChild = viewContainer.createComponent(componentFactory);
      classChild.instance.apiKey = apiKey;
      classChild.instance.selfRef = classChild;
      classChild.instance.name = 'viewClassInfo';
      return classChild;
    }
    return null;
  }
}
