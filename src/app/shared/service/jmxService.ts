import {Observable, of} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {AppUser} from '../../model/appUser';
import {HttpClient} from '@angular/common/http';
import {ApiUrlService} from './apiurl.service';
import {Injectable} from '@angular/core';
import {CurrentUserService} from './currentUserService';
import {ResponseString} from '../../model/responseString';
import {ManagedMachine} from '../../model/managedMachine';
@Injectable()
export default class JmxService {
  constructor(private _http: HttpClient,
              private apiService: ApiUrlService,
              private currentUserService: CurrentUserService) {}

  startConnection(type: string, apiKey: string): Observable<any> {
    return this._http.get<any>(this.apiService.jmxConnections + type + '/' + apiKey, {observe: 'response'});
  }


  closeConnection(type: string, machine: ManagedMachine) {
   // const headers = this.currentUserService.getAuthHeader();
    const close = this._http.delete<boolean>(this.apiService.jmxConnections + type + '/' + machine.apiKey, {observe: 'response'});
    close.subscribe(data => {
    });

  }

  pingConnection(apiKey: string) {
    const ping = this._http.get(this.apiService.jmxConnections + `ping/${apiKey}`);
    ping.subscribe((data) => {});
  }
}
