import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse} from '@angular/common/http';

import {ApiUrlService} from './apiurl.service';
import {CurrentUserService} from './currentUserService';
import {AppUser} from '../../model/appUser';
import {Observable, of} from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import {ResponseString} from '../../model/responseString';


@Injectable()
export class  LoginRegisterService {

  constructor(private _http: HttpClient, private apiService: ApiUrlService, private userService: CurrentUserService) {
  }

  loginUser(username: string, password: string): Observable<any> {

    const UserLoginData: AppUser = new AppUser();
    UserLoginData.username = username;
    UserLoginData.password = password;


    return this._http.post<any>(this.apiService.login, JSON.stringify(UserLoginData), {observe: 'response'});
  }



  getUserRole(username: string): Observable<AppUser> {
    const headers = new HttpHeaders({
      'authorization': this.userService.getUserKey(),
      'content-type': 'application/json'
    });

    return this._http.get<AppUser>(this.apiService.role + username, {headers: headers});

  }


  registerUser(registerDetails: AppUser): Observable<ResponseString> {
    return this._http.post<ResponseString>(this.apiService.users, JSON.stringify(registerDetails));
  }


}
