import {Injectable} from '@angular/core';


@Injectable()
export class  ApiUrlService {


  constructor() {
  }

  public apiRawUrl = 'http://localhost:8080/jmxOnlineApi/';
  public users: string = this.apiRawUrl + 'users/';
  public login: string = this.apiRawUrl + 'login';
  public role: string = this.users + 'getRole/';
  public jmx: string = this.apiRawUrl + 'jmx/';
  public jmxConnections = this.jmx + 'connections/';
  public managedMachines = this.apiRawUrl + 'managedMachines';



}
