import {Injectable} from '@angular/core';
import {MatDialog} from '@angular/material';
import {UniversalDialogComponent} from '../../screen/custom-components/universal-dialog/universal-dialog.component';

@Injectable()
export class ModalService {

  constructor(public dialog: MatDialog) {}

  openDialog(message, title = 'INFO 🙂', willCancelShow= false) {
    return this.dialog.open(UniversalDialogComponent, {
      width: '250px',
      data: {text: message, title: title, isActionOK: false, isActionCancel: false, willCancelShow}
    });

  }

}
