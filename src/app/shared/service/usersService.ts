import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {AppUser} from '../../model/appUser';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {ApiUrlService} from './apiurl.service';
import {CurrentUserService} from './currentUserService';
import {catchError} from 'rxjs/operators';
import {of} from 'rxjs';


@Injectable()
export class UsersService {

  constructor(private _http: HttpClient,
              private _apiUrlService: ApiUrlService,
              private userService: CurrentUserService) {}
  public updateProfile(user: AppUser): Observable<number> {
    return this._http.put<number>(this._apiUrlService.users + 'update', user, )
      .pipe(catchError(this.handleError<any>('updateProfile')));
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console instead
      return of(result as T);
    };
  }


}
