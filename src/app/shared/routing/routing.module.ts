import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {LoginContainerComponent} from '../../container/login-container/login-container.component';
import {MachineListContainerComponent} from '../../container/machine-list-container/machine-list-container.component';
import {MachineDataContainerComponent} from '../../container/machine-data-container/machine-data-container.component';
import {DashboardContainerComponent} from '../../container/dashboard-container/dashboard-container.component';
import {GetStartedContainerComponent} from '../../container/get-started-container/get-started-container.component';
import {RegisterContainerComponent} from '../../container/register-container/register-container.component';



const appRoutes: Routes = [
  {path: 'login', component: LoginContainerComponent},
  {path: 'register', component: RegisterContainerComponent},
  {path: 'dashboard', component: DashboardContainerComponent},
  {path: 'machineList', component: MachineListContainerComponent},
  {path: 'machineDetails/:id', component: MachineDataContainerComponent},
  {path: 'getStarted', component: GetStartedContainerComponent},
  {path: '', component: MachineListContainerComponent},
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes)
  ],
  declarations: [],
  exports: [RouterModule]
})
export class RoutingModule { }
