import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MachineListContainerComponent } from './machine-list-container.component';

describe('MachineListContainerComponent', () => {
  let component: MachineListContainerComponent;
  let fixture: ComponentFixture<MachineListContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MachineListContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachineListContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
