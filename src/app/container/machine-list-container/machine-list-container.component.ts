import { Component, OnInit } from '@angular/core';
import {CurrentUserService} from '../../shared/service/currentUserService';
import {Router} from '@angular/router';
import {ManagedMachineService} from '../../shared/service/managedMachineService';
import {ManagedMachine} from '../../model/managedMachine';
import {ModalService} from '../../shared/service/ModalService';


@Component({
  selector: 'app-machine-list-container',
  templateUrl: './machine-list-container.component.html',
  styleUrls: ['./machine-list-container.component.css']
})
export class MachineListContainerComponent implements OnInit {

  managedMachines: ManagedMachine[];
  constructor(private currentUserService: CurrentUserService,
              private managedMachineService: ManagedMachineService,
              private modalService: ModalService,
              private roter: Router) { }

  ngOnInit() {
    if (!this.currentUserService.isUserLoggedIn()) {
      this.roter.navigate(['/login']);
    } else {
      this.getMachines();
    }

  }
  getMachines() {
    this.managedMachineService.getMachinesForUser()
      .subscribe((data) => {
        this.managedMachines = data;
      });
  }

  onDashboardChange(machine: ManagedMachine) {
    this.managedMachineService.saveOnDashboard(machine).subscribe((data) => {
        this.modalService.openDialog('Machine updated').afterClosed().subscribe((result) => {
          if (result.isActionOK) {
            this.getMachines();
          }
        });
    }, (error => {
      this.modalService.openDialog('Error getting data').afterClosed().subscribe((result) => {
        if (result.isActionOK) {
          this.getMachines();
        }
      });
    }));
  }

}
