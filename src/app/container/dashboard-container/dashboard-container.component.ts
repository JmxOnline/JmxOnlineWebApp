import { Component, OnInit } from '@angular/core';
import MachineSocketService from '../../shared/service/machineSocketService';
import {ManagedMachineService} from '../../shared/service/managedMachineService';
import {ManagedMachine} from '../../model/managedMachine';
import JmxService from '../../shared/service/jmxService';

@Component({
  selector: 'app-dashboard-container',
  templateUrl: './dashboard-container.component.html',
  styleUrls: ['./dashboard-container.component.scss']
})
export class DashboardContainerComponent implements OnInit {

  dashboardMachines: ManagedMachine[];
  constructor(private machineSocketService: MachineSocketService,
              private jmxService: JmxService,
              private managedMachine: ManagedMachineService) { }

  ngOnInit() {
    this.machineSocketService.connectToSocket();
    this.managedMachine.getDashboardMachines().subscribe((data) => {
      this.dashboardMachines = data;
    });
  }



}
