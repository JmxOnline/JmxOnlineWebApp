import { Component, OnInit } from '@angular/core';
import {AppUser} from '../../model/appUser';
import {LoginRegisterService} from '../../shared/service/loginRegister.service';
import {CurrentUserService} from '../../shared/service/currentUserService';
import {ActivatedRoute, Router} from '@angular/router';


@Component({
  selector: 'app-login-container',
  templateUrl: './login-container.component.html',
  styleUrls: ['./login-container.component.css']
})
export class LoginContainerComponent implements OnInit {
  errorData: any;
  constructor(private loginService: LoginRegisterService,
              private routerService: Router,
              private route: ActivatedRoute,
              private currentUserService: CurrentUserService) { }

  ngOnInit() {

    this.route.params.subscribe(params => {
      this.errorData = params['error'];
    });
  }

  onLogin(e: AppUser) {
    this.loginService.loginUser(e.username, e.password)
      .subscribe((data) => {
        this.currentUserService.setUserKey(data.headers.get('Authorization'));
        this.routerService.navigate(['/dashboard']);
      }, error => {
        this.errorData = error.error;
      });
  }

}
