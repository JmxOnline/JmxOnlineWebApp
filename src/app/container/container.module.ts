import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginContainerComponent } from './login-container/login-container.component';
import {ScreenModule} from '../screen/screen.module';
import {MatButtonModule} from '@angular/material';
import {MaterialHandlerModule} from '../shared/material-handler/material-handler.module';
import {CurrentUserService} from '../shared/service/currentUserService';
import {LoginRegisterService} from '../shared/service/loginRegister.service';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {ApiUrlService} from '../shared/service/apiurl.service';
import { HeaderContainerComponent } from './header-container/header-container.component';
import { MachineListContainerComponent } from './machine-list-container/machine-list-container.component';
import { MachineDataContainerComponent } from './machine-data-container/machine-data-container.component';
import {ManagedMachineService} from '../shared/service/managedMachineService';
import { StompRService} from '@stomp/ng2-stompjs';
import { AddMachineContainerComponent } from './add-machine-container/add-machine-container.component';
import {DashboardContainerComponent} from './dashboard-container/dashboard-container.component';
import { GetStartedContainerComponent } from './get-started-container/get-started-container.component';
import { RegisterContainerComponent } from './register-container/register-container.component';
import {ModalService} from '../shared/service/ModalService';
import {UniversalDialogComponent} from '../screen/custom-components/universal-dialog/universal-dialog.component';



@NgModule({
  imports: [
    CommonModule,
    MatButtonModule,
    ScreenModule,
    HttpClientModule
  ],
  declarations: [
    LoginContainerComponent,
    HeaderContainerComponent,
    MachineListContainerComponent,
    MachineDataContainerComponent,
    AddMachineContainerComponent,
    DashboardContainerComponent,
    GetStartedContainerComponent,
    RegisterContainerComponent,
  ],
  providers: [CurrentUserService,
    LoginRegisterService,
    HttpClient,
    ApiUrlService,
    ModalService,
    ManagedMachineService,
    StompRService],
  exports: [MatButtonModule, HeaderContainerComponent],
  entryComponents:[UniversalDialogComponent]
})
export class ContainerModule { }
