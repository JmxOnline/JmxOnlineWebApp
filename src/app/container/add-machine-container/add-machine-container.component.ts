import { Component, OnInit } from '@angular/core';
import {ManagedMachine} from '../../model/managedMachine';
import {CurrentUserService} from '../../shared/service/currentUserService';
import {ManagedMachineService} from '../../shared/service/managedMachineService';
import {ResponseString} from '../../model/responseString';

@Component({
  selector: 'app-add-machine-container',
  templateUrl: './add-machine-container.component.html',
  styleUrls: ['./add-machine-container.component.css']
})
export class AddMachineContainerComponent implements OnInit {

  constructor(private currentUserService: CurrentUserService,
              private  managedMachineService: ManagedMachineService) { }
  jmxKey = '';
  ngOnInit() {
  }
  createMachine(result: ManagedMachine) {
    this.managedMachineService.createNewMachine(result).
      subscribe((jmxKey) => {
        this.jmxKey = jmxKey.response;
    });

  }
}
