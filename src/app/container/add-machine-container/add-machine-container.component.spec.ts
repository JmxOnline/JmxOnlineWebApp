import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMachineContainerComponent } from './add-machine-container.component';

describe('AddMachineContainerComponent', () => {
  let component: AddMachineContainerComponent;
  let fixture: ComponentFixture<AddMachineContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddMachineContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMachineContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
