import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetStartedContainerComponent } from './get-started-container.component';

describe('GetStartedContainerComponent', () => {
  let component: GetStartedContainerComponent;
  let fixture: ComponentFixture<GetStartedContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetStartedContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetStartedContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
