import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-get-started-container',
  templateUrl: './get-started-container.component.html',
  styleUrls: ['./get-started-container.component.css']
})
export class GetStartedContainerComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
