import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MachineDataContainerComponent } from './machine-data-container.component';

describe('MachineDataContainerComponent', () => {
  let component: MachineDataContainerComponent;
  let fixture: ComponentFixture<MachineDataContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MachineDataContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachineDataContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
