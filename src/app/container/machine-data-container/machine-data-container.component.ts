///<reference path="../../../../node_modules/@angular/core/src/metadata/directives.d.ts"/>
import { Component, OnInit } from '@angular/core';
import {CurrentUserService} from '../../shared/service/currentUserService';
import MachineSocketService from '../../shared/service/machineSocketService';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {ManagedMachineService} from '../../shared/service/managedMachineService';
import {filter} from 'rxjs/operators';
import {ManagedMachine} from '../../model/managedMachine';
import JmxService from '../../shared/service/jmxService';

@Component({
  selector: 'app-machine-data-container',
  templateUrl: './machine-data-container.component.html',
  styleUrls: ['./machine-data-container.component.css']
})
export class MachineDataContainerComponent implements OnInit {
machine: ManagedMachine;
  constructor(private currentUserService: CurrentUserService,
              private active: ActivatedRoute,
              private jmxService: JmxService,
              private machineService: ManagedMachineService, ) {
  }

  ngOnInit() {
    this.initComponent();
  }
  private initComponent() {
    if (this.currentUserService.isUserLoggedIn()) {
      this.active.params.subscribe(params => {
        this.machineService.getMachineById(params['id'])
          .subscribe((data) => {
            this.machine = data;
          });
      });
    } else {
    }
  }
  saveConfiguration(machine: ManagedMachine) {

    this.machineService.saveConfiguration(machine).subscribe((data) => {

    });
  }


}
