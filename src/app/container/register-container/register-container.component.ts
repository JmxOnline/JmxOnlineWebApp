import { Component, OnInit } from '@angular/core';
import {UsersService} from '../../shared/service/usersService';
import {LoginRegisterService} from '../../shared/service/loginRegister.service';
import {AppUser} from '../../model/appUser';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register-container',
  templateUrl: './register-container.component.html',
  styleUrls: ['./register-container.component.css']
})
export class RegisterContainerComponent implements OnInit {

  constructor(private loginRegisterService: LoginRegisterService,
              private router: Router) { }

  ngOnInit() {
  }

  register(appUser: AppUser) {
    this.loginRegisterService.registerUser(appUser).subscribe((data) => {
      this.router.navigate(['/login']);
    });
  }

}
