# JmxOnlineApp

## Getting Started

### Prerequisites

* [Node](https://nodejs.org/en/)
* [angular-cli 6.0.8.](https://github.com/angular/angular-cli)
* [npm](https://www.npmjs.com/)

### Installing
open command prompt and cd to the project root

run
```
npm install
```
after that run
```
npm start
```
## Built With

* [angular-cli 6.0.8.](https://github.com/angular/angular-cli)
* [npm 5.0](https://www.npmjs.com/)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

